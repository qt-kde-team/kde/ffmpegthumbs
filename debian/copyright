Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FFmpeg Thumbnailer
Upstream-Contact: kde-multimedia@kde.org
Source: https://download.kde.org/

Files: *
Copyright: 2010, Andreas Scherf <ascherfy@gmail.com>
           2010, Dirk Vanden Boer <dirk.vdb@gmail.com>
           2020, Heiko Schäfer <heiko@rangun.de>
License: GPL-2.0-or-later

Files: cmake/*
Copyright: 2006, Matthias Kretz <kretz@kde.org>
           2008, Alexander Neundorf <neundorf@kde.org>
           2011, Michael Jansen <kde@michael-jansen.biz>
License: BSD-3-clause

Files: debian/*
Copyright: 2013, Pino Toscano <pino@debian.org>
License: GPL-2.0-or-later

Files: org.kde.ffmpegthumbs.metainfo.xml
Copyright: 2019, Jonathan Riddell <jr@jriddell.org>
License: CC0-1.0

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: CC0-1.0
 To the extent possible under law, the author(s) have dedicated all copyright
 and related and neighboring rights to this software to the public domain
 worldwide. This software is distributed without any warranty.
 .
 You should have received a copy of the CC0 Public Domain Dedication along
 with this software. If not, see 
 <https://creativecommons.org/publicdomain/zero/1.0/>.
 --
 On Debian systems, the complete text of the CC0 Public Domain Dedication
 can be found in `/usr/share/common-licenses/CC0-1.0’.

License: GPL-2.0-or-later
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 --
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file `/usr/share/common-licenses/GPL-2’.
